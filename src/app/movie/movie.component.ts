import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})

export class MovieComponent implements OnInit {
  @Input() data:any;
  @Output() delete = new EventEmitter<any>();

  id;
  title;
  studio;
  weekendIncome;
  show = true;

  constructor() { }

  ngOnInit() {
    this.id = this.data.id;
    this.title = this.data.title;
    this.studio = this.data.studio;
    this.weekendIncome = this.data.weekendIncome;
  }

  deleteRow() {
    this.show = false;
    this.delete.emit(this);
  }
}
