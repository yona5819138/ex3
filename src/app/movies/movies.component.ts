import { Component, OnInit } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { MovieComponent } from '../movie/movie.component';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  title: string;
  id: number;
  studio: string;
  weekendIncome:string;
}


@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  name = "no movie";
 
  movies: Tile[]= [
    {cols: 1, rows: 1, color: 'lightblue','id': 1, 'title': 'Spider-Man: Into The Spider-Verse ', 'studio': 'Sony', 'weekendIncome': '$ 35,400,000' },
    {cols: 1, rows: 1, color: 'lightgreen','id': 2, 'title': 'The Mule', 'studio': 'WB', 'weekendIncome': '$ 17,210,000'},
    {cols: 1, rows: 1, color: 'lightpink','id': 3, 'title': "Dr. Seuss' The Grinch (2018)", 'studio': 'Uni', 'weekendIncome': '$ 11,580,000'},
    {cols: 1, rows: 1, color: '#DDBDF1','id': 4, 'title': 'Ralph Breaks the Internet', 'studio': 'BV', 'weekendIncome': '$ 9,589,000'},
    {cols: 1, rows: 1, color: 'lightyellow','id': 5, 'title': 'Mortal Engines', 'studio': 'Uni', 'weekendIncome': '$ 7,501,000'},
   
    
  ];
  
 
  constructor() { }

  ngOnInit() {
    
  }
  
  hideHim(mov) {
    this.name = mov.title;
    console.log(this.name);
    document.getElementById(mov.id).style.display = 'none';
  }
}
